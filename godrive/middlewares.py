from django.core import serializers
# from django.core.files.base import ContentFile
# from django.conf import settings
# from django.core.files.storage import FileSystemStorage
# import calendar
# import time
# import base64
import json
# from Crypto.Cipher import AES
# from hashlib import md5
# from cryptography.fernet import Fernet



def get_clear_array(rowdata):
    jsondata = serializers.serialize('json', rowdata)
    data = json.loads(jsondata)
    result = []
    for obj in data:
        obj['fields']['id'] = obj['pk']
        result.append(obj['fields'])

    return result


# def create_option_list(rowdata,field,key=None):
#     result = []
#     if key is None:
#         for val in rowdata:
#             result.append({
#                 'value': val[field],
#                 'name': val[field]
#             })

#     else:
#         for val in rowdata:
#             result.append({
#                 'value': val[key],
#                 'name': val[field]
#             })

#     return result


def get_clear_object(rowdata):
    jsondata = serializers.serialize('json', [rowdata ,])
    data = json.loads(jsondata)
    result = []
    for obj in data:
        obj['fields']['id'] = obj['pk']
        result.append(obj['fields'])

    return result[0]


# def save_image(data, folder, file_name=None):
#     ts = ''
#     if file_name is None:
#         ts = str(calendar.timegm(time.gmtime()))
#     else:
#         ts = str(file_name)
#     if 'base64' in data:
#         format, img = data.split(';base64,')
#         ext = format.split('/')[-1]
#         ext = "jpg" if ext == "jpeg" else ext
#         complete_file_name = ts+'.'+ext
#         myfile = ContentFile(base64.b64decode(img), name=complete_file_name)
#         print(settings.STATIC_PATH)
#         fs = FileSystemStorage(location=settings.STATIC_PATH+'/uploads/'+folder+'/')
#         filename = fs.save(myfile.name, myfile)
#         file_url = settings.BASE_URL+settings.STATIC_URL+'uploads/'+folder+'/'+filename
#         return file_url
#     else:
#         return data


# def xss_clear(data):
#     return data.replace("<", "").replace(">", "").replace("!", "").replace("*", "")\
#         .replace("~", "").replace("^", "").replace("#", "")


# def xss_check(data):
#     substring = ["<", ">", "!", "*", "~", "^", "#"]
#     count = 1
#     for obj in substring:
#         if obj in str(data):
#             count += 1

#     if count > 1:
#         return False
#     else:
#         return True


# def is_authonticated(request):
#     if request.META.get('HTTP_AUTHORIZATION') :
#         token = request.META.get('HTTP_AUTHORIZATION')
#         try:
#             token_info = UserToken.objects.get(token=token)
#             user = token_info.user
#             try:
#                 user_info = UserRegister.objects.get(id=user.id)
#                 if user_info.is_login == True:
#                     return user_info.id
#                 else :
#                     return None
#             except UserToken.DoesNotExist:
#                 return None
#         except UserToken.DoesNotExist:
#             return None
#     else :
#         return None


# def request_decode(request):
#     rdata = (request.body).decode("utf-8")
#     txt = base64.urlsafe_b64decode(str(rdata) + '=').decode("utf-8")
#     data = json.loads(txt)
#     return data

# def pad (data):
#     BLOCK_SIZE = 16
#     pad = BLOCK_SIZE - len(data) % BLOCK_SIZE
#     return data + pad * chr(pad)

# def unpad (padded):
#     pad = ord(padded[-1])
#     return padded[:-pad]

# def decrypt(edata, password):
#     edata = base64.urlsafe_b64decode(edata)

#     m = md5()
#     m.update(password.encode('utf-8'))
#     key = m.hexdigest()

#     # print(key)
#     m = md5()
#     m.update((password + key).encode('utf-8'))
#     iv = m.hexdigest()

#     # print(iv)
#     aes = AES.new(key.encode('utf8'), AES.MODE_CBC, iv[:16].encode('utf8'))
#     return (aes.decrypt(edata))


# def decrypt_data(request):
#     password = 'mypass'
#     rdata = request.body
#     edata = decrypt(rdata,password)
#     fdata = edata.decode("utf-8")
#     if fdata[0] == '{':
#         final = ( str(fdata) ).replace('":', '": ').replace(',"', ', "')
#         for v in final:
#             print(v)
#         return final
#     else:
#         return (fdata).replace('":"', '": "').replace('","', '", "')


# def encrypt(txt):
#     try:
#         # convert integer etc to string first
#         txt = str(txt)
#         # get the key from settings
#         cipher_suite = Fernet(b'iDJpljxUBBsacCZ50GpSBff6Xem0R-giqXXnBFGJ2Rs=') # key should be byte
#         # #input should be byte, so convert the text to byte
#         encrypted_text = cipher_suite.encrypt(txt.encode('ascii'))
#         # encode to urlsafe base64 format
#         encrypted_text = base64.urlsafe_b64encode(encrypted_text).decode("ascii")
#         return encrypted_text
#     except Exception as e:
#         print(e)
#         return None

# def decrypt(txt):
#     try:
#         # base64 decode
#         txt = base64.urlsafe_b64decode(txt)
#         cipher_suite = Fernet(b'iDJpljxUBBsacCZ50GpSBff6Xem0R-giqXXnBFGJ2Rs=') # key should be byte
#         decoded_text = cipher_suite.decrypt(txt).decode("ascii")
#         return decoded_text
#     except Exception as e:
#         print(e)
#         return None


# def save_files(data, folder, file_name=None):
#     ts = ''
#     if file_name is None:
#         ts = str(calendar.timegm(time.gmtime()))
#     else:
#         ts = str(file_name)
#     if 'base64' in data:
#         format, img = data.split(';base64,')
#         ext = format.split('/')[-1]
#         ext = "jpg" if ext == "jpeg" else ext
#         complete_file_name = ts+'.'+ext
#         myfile = ContentFile(base64.b64decode(img), name=complete_file_name)
#         print(settings.STATIC_PATH)
#         fs = FileSystemStorage(location=settings.STATIC_PATH+'/uploads/'+folder+'/')
#         filename = fs.save(myfile.name, myfile)
#         file_url = settings.BASE_URL+settings.STATIC_URL+'uploads/'+folder+'/'+filename
#         return file_url
#     else:
#         return data