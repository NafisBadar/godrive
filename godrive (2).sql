-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 01, 2021 at 07:06 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `godrive`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add resetpass', 7, 'add_resetpass'),
(26, 'Can change resetpass', 7, 'change_resetpass'),
(27, 'Can delete resetpass', 7, 'delete_resetpass'),
(28, 'Can view resetpass', 7, 'view_resetpass'),
(29, 'Can add vehicle images', 8, 'add_vehicleimages'),
(30, 'Can change vehicle images', 8, 'change_vehicleimages'),
(31, 'Can delete vehicle images', 8, 'delete_vehicleimages'),
(32, 'Can view vehicle images', 8, 'view_vehicleimages'),
(33, 'Can add vehicle', 9, 'add_vehicle'),
(34, 'Can change vehicle', 9, 'change_vehicle'),
(35, 'Can delete vehicle', 9, 'delete_vehicle'),
(36, 'Can view vehicle', 9, 'view_vehicle'),
(37, 'Can add message', 10, 'add_messagemodel'),
(38, 'Can change message', 10, 'change_messagemodel'),
(39, 'Can delete message', 10, 'delete_messagemodel'),
(40, 'Can view message', 10, 'view_messagemodel'),
(41, 'Can add friendlist', 11, 'add_friendlist'),
(42, 'Can change friendlist', 11, 'change_friendlist'),
(43, 'Can delete friendlist', 11, 'delete_friendlist'),
(44, 'Can view friendlist', 11, 'view_friendlist'),
(45, 'Can add friend model', 12, 'add_friendmodel'),
(46, 'Can change friend model', 12, 'change_friendmodel'),
(47, 'Can delete friend model', 12, 'delete_friendmodel'),
(48, 'Can view friend model', 12, 'view_friendmodel'),
(49, 'Can add friend model', 13, 'add_friendmodel'),
(50, 'Can change friend model', 13, 'change_friendmodel'),
(51, 'Can delete friend model', 13, 'delete_friendmodel'),
(52, 'Can view friend model', 13, 'view_friendmodel'),
(53, 'Can add message model', 14, 'add_messagemodel'),
(54, 'Can change message model', 14, 'change_messagemodel'),
(55, 'Can delete message model', 14, 'delete_messagemodel'),
(56, 'Can view message model', 14, 'view_messagemodel');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$216000$c1qytMERb0t4$XBmMHQKIsUiU71gnoSOQzFkB7iwDBF4VlMzmp5C2bhA=', '2021-05-01 18:54:07.085752', 0, 'nafis', '', '', 'nafis.badar@gmail.com', 0, 1, '2021-04-08 11:13:17.801966'),
(2, 'pbkdf2_sha256$216000$7LAptHUssfml$2flhgugwIysiE1yYO8NDexB6maysB8phTsGgPzIjd4g=', '2021-04-08 19:20:37.713701', 0, 'saddam', '', '', 'saddam@gmail.com', 0, 1, '2021-04-08 19:19:15.388731'),
(4, 'pbkdf2_sha256$216000$xyJuF8A01NvU$GZaZW/4mjcMBWV+ljIrKuTq8/AdwaDUT01fJbMk07XY=', '2021-04-25 15:37:20.685715', 0, 'xyz', '', '', 'xyz@gmail.com', 0, 1, '2021-04-11 07:30:46.382178'),
(5, 'pbkdf2_sha256$216000$dzALGDyJhTfz$mH7vm5boDLCuvFHzmxiEAGAUzioLuzuYFAicLNwWF+M=', '2021-04-18 12:07:27.487134', 1, 'admin', '', '', '', 1, 1, '2021-04-18 12:06:14.952669'),
(6, 'pbkdf2_sha256$216000$Nd17bNKKVxzk$JgdOpTa4X4sfzQl4lfATTc8KR4YtIcUukzEmJMPEFcc=', '2021-04-18 16:06:05.632044', 0, 'saif', '', '', 'tariquealam29@gmail.com', 0, 1, '2021-04-18 16:06:03.423307');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chat_friendmodel`
--

CREATE TABLE `chat_friendmodel` (
  `id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chat_friendmodel`
--

INSERT INTO `chat_friendmodel` (`id`, `friend_id`, `user_id`) VALUES
(7, 2, 1),
(8, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2021-04-18 12:10:08.761779', '5', 'VehicleImages object (5)', 1, '[{\"added\": {}}]', 8, 5),
(2, '2021-04-18 12:17:28.382606', '7', 'VehicleImages object (7)', 1, '[{\"added\": {}}]', 8, 5),
(3, '2021-04-18 12:21:34.446619', '9', 'VehicleImages object (9)', 1, '[{\"added\": {}}]', 8, 5);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(12, 'chat', 'friendmodel'),
(10, 'chat', 'messagemodel'),
(5, 'contenttypes', 'contenttype'),
(11, 'homelink', 'friendlist'),
(13, 'homelink', 'friendmodel'),
(14, 'homelink', 'messagemodel'),
(7, 'homelink', 'resetpass'),
(9, 'homelink', 'vehicle'),
(8, 'homelink', 'vehicleimages'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2021-04-05 19:17:37.726372'),
(2, 'auth', '0001_initial', '2021-04-05 19:17:44.161695'),
(3, 'admin', '0001_initial', '2021-04-05 19:18:04.189778'),
(4, 'admin', '0002_logentry_remove_auto_add', '2021-04-05 19:18:08.111537'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2021-04-05 19:18:08.446348'),
(6, 'contenttypes', '0002_remove_content_type_name', '2021-04-05 19:18:10.213338'),
(7, 'auth', '0002_alter_permission_name_max_length', '2021-04-05 19:18:12.620963'),
(8, 'auth', '0003_alter_user_email_max_length', '2021-04-05 19:18:13.179642'),
(9, 'auth', '0004_alter_user_username_opts', '2021-04-05 19:18:13.467477'),
(10, 'auth', '0005_alter_user_last_login_null', '2021-04-05 19:18:15.223477'),
(11, 'auth', '0006_require_contenttypes_0002', '2021-04-05 19:18:15.267470'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2021-04-05 19:18:15.313433'),
(13, 'auth', '0008_alter_user_username_max_length', '2021-04-05 19:18:15.590286'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2021-04-05 19:18:15.789151'),
(15, 'auth', '0010_alter_group_name_max_length', '2021-04-05 19:18:16.201914'),
(16, 'auth', '0011_update_proxy_permissions', '2021-04-05 19:18:16.257887'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2021-04-05 19:18:16.425793'),
(18, 'sessions', '0001_initial', '2021-04-05 19:18:17.399233'),
(19, 'homelink', '0001_initial', '2021-04-17 07:57:19.575008'),
(20, 'homelink', '0002_vehicle_vehicleimages', '2021-04-18 10:57:53.560218'),
(21, 'homelink', '0003_auto_20210418_1652', '2021-04-18 11:22:19.544207'),
(22, 'homelink', '0004_auto_20210418_2035', '2021-04-18 15:05:33.593105'),
(23, 'homelink', '0005_auto_20210421_2149', '2021-04-21 16:19:26.268444'),
(24, 'homelink', '0006_auto_20210421_2202', '2021-04-21 16:32:54.830848'),
(25, 'chat', '0001_initial', '2021-04-22 16:37:48.928084'),
(26, 'homelink', '0007_friendlist', '2021-04-22 17:58:58.154998'),
(27, 'homelink', '0008_auto_20210423_1135', '2021-04-23 06:05:48.827567'),
(28, 'homelink', '0009_delete_friendlist', '2021-04-24 13:31:15.832325'),
(29, 'chat', '0002_friendmodel', '2021-04-24 14:29:38.201426'),
(30, 'homelink', '0010_friendmodel', '2021-04-30 18:24:10.054979'),
(31, 'homelink', '0011_messagemodel', '2021-05-01 06:06:25.274033'),
(32, 'homelink', '0012_delete_messagemodel', '2021-05-01 06:07:43.483709'),
(33, 'homelink', '0013_messagemodel', '2021-05-01 06:09:07.621347'),
(34, 'homelink', '0010_friendmodel_messagemodel', '2021-05-01 18:45:09.092293');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('3t8kkxbotz3i1c5z5e2ac5la4lates5d', '.eJxVjMsKwyAQAP_FcxGDWVd77L3fILs-atqiEJNT6L8XIYf2OjPMITztW_F7T6tforiKSVx-GVN4pTpEfFJ9NBla3daF5Ujkabu8t5jet7P9GxTqZWyzoojAhABmtmCUJYgW3cSoyOSgNVtIOjjmADnNkNE5A5gJ0YVZfL7Z4jfJ:1lagyP:L5-gEiuVeMX5wyKfWTJfsQm9ypY6D3hiHvT0423U-8A', '2021-05-09 15:47:05.983591'),
('42at4d7qzy2i7q49fp2fy64dn92o4ij8', '.eJxVjMsKwyAQAP_FcxGDWVd77L3fILs-atqiEJNT6L8XIYf2OjPMITztW_F7T6tforiKSVx-GVN4pTpEfFJ9NBla3daF5Ujkabu8t5jet7P9GxTqZWyzoojAhABmtmCUJYgW3cSoyOSgNVtIOjjmADnNkNE5A5gJ0YVZfL7Z4jfJ:1lcukh:QnsK2jr-sj11vc941hxFKBPpJ5JPT4WdKOdW5BAIGL8', '2021-05-15 18:54:07.165674'),
('4x4p9g9slxlqu4baz487us36rwambxqq', '.eJxVjMsKwyAQAP_FcxGDWVd77L3fILs-atqiEJNT6L8XIYf2OjPMITztW_F7T6tforiKSVx-GVN4pTpEfFJ9NBla3daF5Ujkabu8t5jet7P9GxTqZWyzoojAhABmtmCUJYgW3cSoyOSgNVtIOjjmADnNkNE5A5gJ0YVZfL7Z4jfJ:1laIXS:THITegd74c64m9A6aGNH9Y3GaIQJfMXrjC6EtonfT9I', '2021-05-08 13:41:38.256735'),
('k8lbwaxm8zbuwt0wh84yt5dm778spztu', '.eJxVjMsKwyAQAP_FcxGDWVd77L3fILs-atqiEJNT6L8XIYf2OjPMITztW_F7T6tforiKSVx-GVN4pTpEfFJ9NBla3daF5Ujkabu8t5jet7P9GxTqZWyzoojAhABmtmCUJYgW3cSoyOSgNVtIOjjmADnNkNE5A5gJ0YVZfL7Z4jfJ:1lZYr1:Fa5_Jp7yl9xoe_8hBeWaULuoWwHgddB7LrAMtalBHkI', '2021-05-06 12:54:47.422391'),
('m7ygscp6mtfm1xr66im0gt8zxv9mfo8l', 'e30:1lY1hZ:bkKhU3_tkMXBOhcva2cIKXuMEU4s0wXSyFMhnrKDV54', '2021-05-02 07:18:41.974507'),
('n15ukezjthgfyzxll22ar35yu67ss5ps', '.eJxVjMsKwyAQAP_FcxGDWVd77L3fILs-atqiEJNT6L8XIYf2OjPMITztW_F7T6tforiKSVx-GVN4pTpEfFJ9NBla3daF5Ujkabu8t5jet7P9GxTqZWyzoojAhABmtmCUJYgW3cSoyOSgNVtIOjjmADnNkNE5A5gJ0YVZfL7Z4jfJ:1lYvYq:8MLdbKk98liS1apWbAXOx9ArOLESs0f3vBD7z0YsSIk', '2021-05-04 18:57:24.507310'),
('q71t8kkzao4xwdmi3stxt2i7qg2rmqfs', '.eJxVjDsOwjAQBe_iGlne-E9JnzNYu_GCA8iW4qRC3B0ipYD2zcx7iYTbWtLWeUlzFmcB4vS7EU4PrjvId6y3JqdW12UmuSvyoF2OLfPzcrh_BwV7-dYOlY7gwGtQzjBBHmIIhBad1hR8ADcEJOOUJzbWqMjqaklTjoTgQbw_rOU21A:1lYA3B:8Rl8KGFPOXcwKNg9a26PIGwsdYe7ca-gIrZLb2TcCos', '2021-05-02 16:13:33.996990'),
('qofj1lltukvylorq23wkmje75a9yyc5v', '.eJxVjMsKwyAQAP_FcxGDWVd77L3fILs-atqiEJNT6L8XIYf2OjPMITztW_F7T6tforiKSVx-GVN4pTpEfFJ9NBla3daF5Ujkabu8t5jet7P9GxTqZWyzoojAhABmtmCUJYgW3cSoyOSgNVtIOjjmADnNkNE5A5gJ0YVZfL7Z4jfJ:1lZbMF:hS8AcOikV82B8_OgqIaUVxoet8De8w2zMyx0lowcw7c', '2021-05-06 15:35:11.062615'),
('rq2ufr4hfxchd2ubmsb63cu7zngeyv1g', '.eJxVjMsKwyAQAP_FcxGDWVd77L3fILs-atqiEJNT6L8XIYf2OjPMITztW_F7T6tforiKSVx-GVN4pTpEfFJ9NBla3daF5Ujkabu8t5jet7P9GxTqZWyzoojAhABmtmCUJYgW3cSoyOSgNVtIOjjmADnNkNE5A5gJ0YVZfL7Z4jfJ:1lZcqF:RGW2RwGwjzkfrYXGwStZVo2ZG6GllboTPPW3IDIa5ug', '2021-05-06 17:10:15.296284'),
('wkb2wjq03hgbosjvoenuut9mebfgqx3b', '.eJxVjMsKwyAQAP_FcxGDWVd77L3fILs-atqiEJNT6L8XIYf2OjPMITztW_F7T6tforiKSVx-GVN4pTpEfFJ9NBla3daF5Ujkabu8t5jet7P9GxTqZWyzoojAhABmtmCUJYgW3cSoyOSgNVtIOjjmADnNkNE5A5gJ0YVZfL7Z4jfJ:1lagRy:AyCJGknQDOFq4N3SPYWlHLf1vGzPFGRcPISsRYWk5bY', '2021-05-09 15:13:34.219955'),
('yh4ed2gyao7swbjp78zwoiomv613sfqe', 'e30:1lY1hc:mT20x6jpIy_74lpUwoq02kYv4kpquDU2eazf8tGq-pY', '2021-05-02 07:18:44.638981');

-- --------------------------------------------------------

--
-- Table structure for table `homelink_friendmodel`
--

CREATE TABLE `homelink_friendmodel` (
  `id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `homelink_friendmodel`
--

INSERT INTO `homelink_friendmodel` (`id`, `friend_id`, `user_id`) VALUES
(1, 2, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `homelink_messagemodel`
--

CREATE TABLE `homelink_messagemodel` (
  `id` int(11) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `body` longtext NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `homelink_messagemodel`
--

INSERT INTO `homelink_messagemodel` (`id`, `timestamp`, `body`, `recipient_id`, `user_id`) VALUES
(1, '2021-05-01 18:57:32.952085', 'hello', 2, 1),
(2, '2021-05-01 18:57:43.089950', 'facebook', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `homelink_resetpass`
--

CREATE TABLE `homelink_resetpass` (
  `id` int(11) NOT NULL,
  `uid` varchar(10) NOT NULL,
  `token` longtext NOT NULL,
  `is_used` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `homelink_resetpass`
--

INSERT INTO `homelink_resetpass` (`id`, `uid`, `token`, `is_used`, `user_id`) VALUES
(1, 'MQ', 'ccd31be7-fc6a-4f9e-9cfc-60f3be656da4', 0, 1),
(2, 'Ng', '896ac2eb-cf80-49c2-b398-93d0b73335a5', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `homelink_vehicle`
--

CREATE TABLE `homelink_vehicle` (
  `id` int(11) NOT NULL,
  `categories` varchar(100) NOT NULL,
  `vehicle_brand` varchar(100) NOT NULL,
  `yearofmaking` varchar(100) NOT NULL,
  `fueltype` varchar(100) NOT NULL,
  `transmission` varchar(100) NOT NULL,
  `reg_no` varchar(100) NOT NULL,
  `km_driven` varchar(100) NOT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  `keyfeature` varchar(100) NOT NULL,
  `add_info` varchar(100) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_on` datetime(6) NOT NULL,
  `updated_on` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `homelink_vehicle`
--

INSERT INTO `homelink_vehicle` (`id`, `categories`, `vehicle_brand`, `yearofmaking`, `fueltype`, `transmission`, `reg_no`, `km_driven`, `start_date`, `end_date`, `keyfeature`, `add_info`, `is_deleted`, `created_on`, `updated_on`, `user_id`, `price`) VALUES
(4, 'Car', 'Hyundai', '2010', 'Petrol', 'Automatic', '38042038832', '1500', '2021-04-21 22:47:00.000000', '2021-04-24 22:47:00.000000', 'Pets Friendly', 'This car is ready for giving on rent', 0, '2021-04-21 22:49:19.556861', '2021-04-21 22:49:19.556861', 2, 500),
(5, 'Car', 'VW', '2012', 'Diesel', 'Automatic', '9556324', '18000', '2021-04-01 22:49:00.000000', '2021-04-21 22:50:00.000000', 'Wireless Internet', 'This car is good and have a multiple connection options', 0, '2021-04-21 22:51:11.548986', '2021-04-21 22:51:11.548986', 2, 800),
(6, 'Car', 'VW', '2010', 'Petrol', 'Automatic', '38042', '20000', '2021-04-21 22:51:00.000000', '2021-04-22 22:51:00.000000', 'Accepts Credit Cards', 'Enjpy your RIde with no cash in your pocket', 0, '2021-04-21 22:52:42.310617', '2021-04-21 22:52:42.310617', 4, 500),
(7, 'Bike', 'Hyundai', '2010', 'Petrol', 'Manual', '9897454', '15000', '2021-03-26 22:53:00.000000', '2021-04-22 22:53:00.000000', 'Smoking Allowed', 'This car is good for outing', 0, '2021-04-21 22:54:15.561305', '2021-04-21 22:54:15.561305', 2, 400);

-- --------------------------------------------------------

--
-- Table structure for table `homelink_vehicleimages`
--

CREATE TABLE `homelink_vehicleimages` (
  `id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `homelink_vehicleimages`
--

INSERT INTO `homelink_vehicleimages` (`id`, `vehicle_id`, `image`) VALUES
(6, 4, 'best-5-suvs-to-buy-this-diwali.jpg'),
(7, 5, 'hyundai_creta.jpg'),
(8, 6, 'ion.jpg'),
(9, 7, 'jeep.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `chat_friendmodel`
--
ALTER TABLE `chat_friendmodel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chat_friendmodel_friend_id_465e4ddb_fk_auth_user_id` (`friend_id`),
  ADD KEY `chat_friendmodel_user_id_66ec45ff_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `homelink_friendmodel`
--
ALTER TABLE `homelink_friendmodel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `homelink_friendmodel_friend_id_08f25b8d_fk_auth_user_id` (`friend_id`),
  ADD KEY `homelink_friendmodel_user_id_f982db98_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `homelink_messagemodel`
--
ALTER TABLE `homelink_messagemodel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `homelink_messagemodel_recipient_id_ffae1051_fk_auth_user_id` (`recipient_id`),
  ADD KEY `homelink_messagemodel_user_id_d3da6413_fk_auth_user_id` (`user_id`),
  ADD KEY `homelink_messagemodel_timestamp_2f54d2f8` (`timestamp`);

--
-- Indexes for table `homelink_resetpass`
--
ALTER TABLE `homelink_resetpass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `homelink_resetpass_user_id_86a60f4d_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `homelink_vehicle`
--
ALTER TABLE `homelink_vehicle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `homelink_vehicle_user_id_52bd95de_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `homelink_vehicleimages`
--
ALTER TABLE `homelink_vehicleimages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `homelink_vehicleimag_vehicle_id_128dfafc_fk_homelink_` (`vehicle_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_friendmodel`
--
ALTER TABLE `chat_friendmodel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `homelink_friendmodel`
--
ALTER TABLE `homelink_friendmodel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `homelink_messagemodel`
--
ALTER TABLE `homelink_messagemodel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `homelink_resetpass`
--
ALTER TABLE `homelink_resetpass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `homelink_vehicle`
--
ALTER TABLE `homelink_vehicle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `homelink_vehicleimages`
--
ALTER TABLE `homelink_vehicleimages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `chat_friendmodel`
--
ALTER TABLE `chat_friendmodel`
  ADD CONSTRAINT `chat_friendmodel_friend_id_465e4ddb_fk_auth_user_id` FOREIGN KEY (`friend_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `chat_friendmodel_user_id_66ec45ff_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `homelink_friendmodel`
--
ALTER TABLE `homelink_friendmodel`
  ADD CONSTRAINT `homelink_friendmodel_friend_id_08f25b8d_fk_auth_user_id` FOREIGN KEY (`friend_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `homelink_friendmodel_user_id_f982db98_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `homelink_messagemodel`
--
ALTER TABLE `homelink_messagemodel`
  ADD CONSTRAINT `homelink_messagemodel_recipient_id_ffae1051_fk_auth_user_id` FOREIGN KEY (`recipient_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `homelink_messagemodel_user_id_d3da6413_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `homelink_resetpass`
--
ALTER TABLE `homelink_resetpass`
  ADD CONSTRAINT `homelink_resetpass_user_id_86a60f4d_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `homelink_vehicle`
--
ALTER TABLE `homelink_vehicle`
  ADD CONSTRAINT `homelink_vehicle_user_id_52bd95de_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `homelink_vehicleimages`
--
ALTER TABLE `homelink_vehicleimages`
  ADD CONSTRAINT `homelink_vehicleimag_vehicle_id_128dfafc_fk_homelink_` FOREIGN KEY (`vehicle_id`) REFERENCES `homelink_vehicle` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
