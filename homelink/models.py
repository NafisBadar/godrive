from django.db import models
from datetime import datetime, date
from django.contrib.auth.models import User

from django.db.models import (Model, TextField, DateTimeField, ForeignKey,
                              CASCADE)

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer



class Resetpass(models.Model):
    uid=models.CharField(max_length=10)
    token=models.TextField(max_length=100,blank=True)
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    is_used=models.BooleanField(default=0)

    def __str__(self):
        return self.user

class Vehicle(models.Model):
    categories = models.CharField(max_length=100)
    vehicle_brand = models.CharField(max_length=100)
    yearofmaking = models.CharField(max_length=100)
    fueltype = models.CharField(max_length=100)
    transmission = models.CharField(max_length=100)
    reg_no = models.CharField(max_length=100)
    km_driven = models.CharField(max_length=100)
    price = models.IntegerField()
    start_date=models.DateTimeField(blank=True, null=True)
    end_date=models.DateTimeField(blank=True, null=True)
    keyfeature=models.CharField(max_length=100)
    add_info=models.CharField(max_length=100)
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    is_deleted=models.BooleanField(default=0)
    created_on = models.DateTimeField(default=datetime.now)
    updated_on = models.DateTimeField(default=datetime.now)


    def __str__(self):
        return self.categories

class VehicleImages(models.Model):
    vehicle=models.ForeignKey(Vehicle,on_delete=models.CASCADE)
    image = models.ImageField(null=False, blank=False)




class FriendModel(Model):
    user = ForeignKey(User, on_delete=CASCADE, verbose_name='user',
                      related_name='user', db_index=True)
    friend = ForeignKey(User, on_delete=CASCADE, verbose_name='friend',
                           related_name='friend_req', db_index=True)

class MessageModel(Model):
    """
    This class represents a chat message. It has a owner (user), timestamp and
    the message body.

    """
    user = ForeignKey(User, on_delete=CASCADE, verbose_name='user',
                      related_name='from_user1', db_index=True)
    recipient = ForeignKey(User, on_delete=CASCADE, verbose_name='recipient',
                           related_name='to_user1', db_index=True)
    timestamp = DateTimeField('timestamp', auto_now_add=True, editable=False,
                              db_index=True)
    body = TextField('body')

    def __str__(self):
        return str(self.id)

    def characters(self):
        """
        Toy function to count body characters.
        :return: body's char number
        """
        return len(self.body)

    def notify_ws_clients(self):
        """
        Inform client there is a new message.
        """
        notification = {
            'type': 'recieve_group_message',
            'message': '{}'.format(self.id)
        }

        channel_layer = get_channel_layer()
        print("user.id {}".format(self.user.id))
        print("user.id {}".format(self.recipient.id))

        async_to_sync(channel_layer.group_send)("{}".format(self.user.id), notification)
        async_to_sync(channel_layer.group_send)("{}".format(self.recipient.id), notification)

    def save(self, *args, **kwargs):
        """
        Trims white spaces, saves the message and notifies the recipient via WS
        if the message is new.
        """
        new = self.id
        self.body = self.body.strip()  # Trimming whitespaces from the body
        super(MessageModel, self).save(*args, **kwargs)
        if new is None:
            self.notify_ws_clients()

    # Meta
    class Meta:
        app_label = 'homelink'
        verbose_name = 'message'
        verbose_name_plural = 'messages'
        ordering = ('-timestamp',)

