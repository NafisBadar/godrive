# Generated by Django 3.1.7 on 2021-04-18 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homelink', '0003_auto_20210418_1652'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vehicleimages',
            name='images',
        ),
        migrations.AddField(
            model_name='vehicleimages',
            name='image',
            field=models.ImageField(default=1, upload_to=''),
            preserve_default=False,
        ),
    ]
