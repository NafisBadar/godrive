
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib import auth
import uuid
from . models import FriendModel
from . models import Resetpass,Vehicle,VehicleImages
from django.contrib.auth.decorators import login_required
import datetime

from godrive.middlewares import get_clear_array,get_clear_object
from django.shortcuts import render, redirect
from django.core.mail import send_mail, BadHeaderError,EmailMessage
from django.template.loader import render_to_string,get_template
from django.db.models.query_utils import Q
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.contrib import messages #import messages

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db import IntegrityError


@csrf_exempt
def index(request):
    if request.method == "GET":
        return render(request, 'index.html')
    else:
        data = request.POST
        print(data)
        login_username = data.get('login_username', None)
        login_password = data.get('login_password', None)
        register_username = data.get('register_username', None)
        register_email = data.get('register_email', None)
        register_password = data.get('register_password', None)
        register_confirm_password = data.get('register_confirm_password', None)
        forget_email = data.get('forget_email', None)
        print(register_username, register_password, register_email, None)
        if login_username and login_password is not None:
            user = authenticate(
                request, username=login_username, password=login_password)
            if user is None:
                return render(request, 'index.html', {'error': 'This Username or Password incorrect'})
            else:
                login(request, user)
                print(user.username)
                return render(request, 'index.html')
        if register_username and register_email and register_password is not None:
            if register_password == register_confirm_password:
                try:
                    user = User.objects.create_user(
                        username=register_username, email=register_email, password=register_password)
                    user.save()
                    login(request, user)
                    return render(request, 'index.html')
                except IntegrityError:
                    return render(request, 'index.html', {'error': 'This Username has been already taken'})
            else:
                return render(request, 'signup.html', {'error': 'Both password not matched'})
        if forget_email is not None:
            print("xyz")
            associated_users = User.objects.filter(Q(email=forget_email))
            print(associated_users)
            if associated_users.exists():
                for user in associated_users:
                    print(user)
                    token_gen=str(uuid.uuid4())
                    uid=urlsafe_base64_encode(force_bytes(user.pk))
                    subject = "Password Reset Requested"
                    # email_template_name = "password_reset_email.txt"
                    email_template_name = "mailtemp.html"
                    c = {
                        "email": user.email,
                        'domain': '127.0.0.1:8000',
                        'site_name': 'Website',
                        "uid":uid,
                        "user": user,
                        'token': token_gen,
                        'protocol': 'http'
                    }
                    print(c)
                    print(uuid.uuid4())

                    
                    # email = render_to_string(email_template_name, c)
                    email=get_template('mailtemp.html').render(c)

                    messages= reset=reset_password_fun(user,token_gen,uid)
                    if messages =="success":
                        try:
                            msg=EmailMessage(subject, email, 'nafistestlogix@gmail.com',
                                    [user.email])
                            msg.content_subtype = "html"
                            msg.send()
                        except BadHeaderError:
                            return HttpResponse('Invalid header found.')
                    else:
                        return HttpResponse("Some thing gone bad")
            return render(request=request, template_name="password_reset.html")


def logout(request):
    auth.logout(request)
    return redirect('home')


def about(request):
    return render(request, 'about.html')
    # return redirect('chat')

@csrf_exempt
def search_list(request):
    if request.method=="GET":
        vehicle_details=[]
        vehicle=Vehicle.objects.all()
        vehicle_clean=get_clear_array(vehicle)
        for i in vehicle_clean:   
            image=VehicleImages.objects.filter(vehicle=i['id'])[:1]
            i['images']=image
            vehicle_details.append(i)
        return render(request, 'search-list.html',{'data':vehicle_details})
    else:
        data= request.POST
        print(data)
        categories=data.get('categories',None)
        hightolow=data.get('hightolow',None)
        lowtohigh=data.get('lowtohigh',None)
        transmission=data.get('transmission',None)
        max_value=data.get('price',None)
        min_value=1
        v_price=""
        if hightolow == "on":
            v_price = '-price'
        else:
            v_price = 'price'
            
            
        vehicle_details=[]

        vehicle=Vehicle.objects.filter(price__range=[min_value,max_value]).order_by(v_price)
        # print(vehicle)
        filter_vehicle=""
        if categories =="None" and transmission == "None":
            filter_vehicle=vehicle
            print(vehicle)
        elif categories is not None and categories != "None":
            filter_vehicle = vehicle.filter(categories=categories)
            if transmission is not None and transmission != "None":
                filter_vehicle=filter_vehicle.filter(transmission=transmission)
        elif categories=="None" and transmission is not None:
            filter_vehicle=vehicle.filter(transmission=transmission)
        
        vehicle_clean=get_clear_array(filter_vehicle)
        for i in vehicle_clean:   
            image=VehicleImages.objects.filter(vehicle=i['id'])[:1]
            i['images']=image
            vehicle_details.append(i)
            print(vehicle_details)
        return render(request, 'search-list.html',{'data':vehicle_details})



def privacy_policy(request):
    return render(request, 'privacy-policy.html')


def tnc_condition(request):
    return render(request, 'terms-and-conditions.html')


@csrf_exempt
def user_login(request):
    if request.method == "POST":
        data = request.POST
        print(data)
        login_username = data.get('login_username', None)
        login_password = data.get('login_password', None)
        register_username = data.get('register_username', None)
        register_email = data.get('register_email', None)
        register_password = data.get('register_password', None)
        register_confirm_password = data.get('register_confirm_password', None)
        print(register_username, register_password, register_email, None)
        if login_username and login_password is not None:
            user = authenticate(
                request, username=login_username, password=login_password)
            if user is None:
                return render(request, 'index.html', {'error': 'This Username or Password incorrect'})
            else:
                login(request, user)
                print(user.username)
                return render(request, 'index.html')
        if register_username and register_email and register_password is not None:
            if register_password == register_confirm_password:
                try:
                    user = User.objects.create_user(
                        username=register_username, email=register_email, password=register_password)
                    user.save()
                    login(request, user)
                    return render(request, 'index.html')
                except IntegrityError:
                    return render(request, 'index.html', {'error': 'This Username has been already taken'})
            else:
                return render(request, 'signup.html', {'error': 'Both password not matched'})

@csrf_exempt
def password_reset_confirm(request,uidb64,token):
    print(uidb64,token)
    if uidb64 and token is not None:
        user_id=Resetpass.objects.filter(uid=uidb64,token=token,is_used=0).values('user')
        print(user_id)
        if user_id.exists():
            if request.method=="GET":
                return render (request,'confirm_pass_reset.html')
            else:
                data = request.POST
                res_password = data.get('pswd1', None)
                print(res_password)

                for user_i in user_id:
                    # Resetpass.objects.filter(uid=uidb64,token=token).update(is_used=1)
                    print(user_id)
                    
                    # update_pasword=User.objects.filter(id=user_i['user']).update(password=user.set_password(res_password))
                    users=User.objects.filter(id=user_i['user'])
                    user=users[0]
                    user.set_password(res_password)
                    user.save()
                    return redirect('home')

        else:
            return HttpResponse( "ERROR 404")

    return HttpResponse("not done")

@csrf_exempt
@login_required(login_url='home')
def upload_vehicle(request):
    if request.method=="GET":
        return render (request,'user-upload-form.html')
    else:
        print(request.FILES)
        data = request.POST
        user=request.user
        categories = data.get('categories', None)
        vehicle_brand = data.get('vehicle_brand', None)
        yearofmaking = data.get('yearofmaking', None)
        fueltype = data.get('fueltype', None)
        transmission = data.get('transmission', None)
        reg_no = data.get('reg_no', None)
        km_driven = data.get('km_driven', None)
        price = data.get('price',None)
        start_date=data.get('start_Date',None)
        exact_start_date=datetime.datetime.strptime(start_date,"%Y-%m-%dT%H:%M")
        print(exact_start_date)
        end_date=data.get('end_date',None)
        exact_end_date=datetime.datetime.strptime(end_date,"%Y-%m-%dT%H:%M")
        print(exact_end_date)
        keyfeature=data.get('keyfeature',None)
        add_info=data.get('add_info',None)
        images = request.FILES.getlist('images')

        upload_vehicle=Vehicle.objects.create(
            user=request.user,
            categories=categories,
            vehicle_brand=vehicle_brand,
            yearofmaking=yearofmaking,
            fueltype=fueltype,
            transmission=transmission,
            reg_no=reg_no,
            km_driven=km_driven,
            price=price,
            start_date=start_date,
            end_date=end_date,
            keyfeature=keyfeature,
            add_info=add_info
        )
        for image in images:
            upload_image=VehicleImages.objects.create(
                vehicle=upload_vehicle,
                image=image
            )
        
        return render (request,'user-upload-form.html')

def vehicle_detail(request,id):
    vehicle_details=[]
    vehicle=Vehicle.objects.filter(id=id)
    vehicle_clean=get_clear_array(vehicle)
    for i in vehicle_clean:
        user_id=i['user']
        username=User.objects.filter(id=user_id)
        username_clean=get_clear_array(username)
        i['username']=username_clean[0]['username']

    for i in vehicle_clean:   
        image=VehicleImages.objects.filter(vehicle=i['id'])
        i['images']=image
        vehicle_details.append(i)
        
    # image=VehicleImages.objects.all()
    print(vehicle_clean)

    return render(request, 'vehicle_details.html',{'data':vehicle_details})


def reset_password_fun(user_id,token_id,uid_id):
    if user_id is not None:
        associated_users = Resetpass.objects.filter(Q(user=user_id))
        if associated_users.exists():
            
            token_update=Resetpass.objects.filter(user=user_id).update(uid=uid_id,token=token_id)
            return "success"
        else:
            token_save=Resetpass.objects.create(
                user=user_id,
                uid=uid_id,
                token=token_id
            )
            
            token_save.save()
            return "success"
    else:
        return "failed"

@csrf_exempt
@login_required
def chatfriend(request,id):
    print(id)
    frienduser=User.objects.filter(id=id)
    for i in frienduser:
        friend=FriendModel.objects.filter(user=request.user,friend=i)
        print(friend,"this is inside of friend cheking")
        if friend is not None and len(friend)!=0:
            return redirect('chat')
        else:
            try:
                print(request.user,i,"this is inside the friend")
                insert_query=FriendModel.objects.create(user=request.user,friend=i)
                print("one executed")
                insert_query1=FriendModel.objects.create(user=i,friend=request.user)
                return redirect('chat')
            except:
                return HttpResponse("Error 404")

    



        

# @csrf_exempt
# def password_reset_request(request):
# 	if request.method == "POST":
#         email=request.POST['email']
# 		if email is not None:
        #         associated_users = User.objects.filter(Q(email=data))
        #         if associated_users.exists():
        #             for user in associated_users:
        #                 subject = "Password Reset Requested"
        #                 email_template_name = "templates/password_reset_email.txt"
        #                 c = {
        #                       "email": user.email,
        #                       'domain': '127.0.0.1:8000',
        #                         'site_name': 'Website',
        #                         "uid": urlsafe_base64_encode(force_bytes(user.pk)),
        #                         "user": user,
        #                         'token': default_token_generator.make_token(user),
        #                         'protocol': 'http',
        #                       }
        #                   email = render_to_string(email_template_name, c)
        #                    try:
        #                         send_mail(subject, email, 'admin@example.com',
        #                                   [user.email], fail_silently=False)
        #                     except BadHeaderError:
        #                         return HttpResponse('Invalid header found.')
        #                     return HttpResponse("this has been done")
        # return render(request=request, template_name="main/password/password_reset.html")
