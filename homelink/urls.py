from django.contrib import admin
from django.urls import path,include
from .import views
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from rest_framework.routers import DefaultRouter
from . api import MessageModelViewSet, UserModelViewSet

router = DefaultRouter()
router.register(r'message', MessageModelViewSet, basename='message-api')
router.register(r'user', UserModelViewSet, basename='user-api')


urlpatterns = [
    path('', views.index,name='home'),
    path('logout',views.logout,name='logout'),
    path('about',views.about,name='about'),
    path('search-list',views.search_list,name='search-list'),
    path('tnc',views.tnc_condition,name='tnc'),
    path('login',views.user_login,name='login'),
    path('privacy-policy',views.privacy_policy,name='privacy-policy'),
    path('reset/<uidb64>/<token>/',views.password_reset_confirm, name='password_reset_confirm'),
    path('upload-vehicle',views.upload_vehicle,name='upload-vehicle'),
    path('vehicle_detail/<int:id>',views.vehicle_detail,name="vehicle_details"),
    path('vehicle_detail/chatfriend/<int:id>',views.chatfriend,name="chatfriend"),


    #chat
    path(r'chat/api/v1/', include(router.urls)),

    path('chat/', login_required(
        TemplateView.as_view(template_name='chat/core/chat.html')), name='chat'),
    
]